const aliases = {
	'haven.ItemInfo$Name' : 'n',
	'Quality' : 'q',
};
const aliasesExtra = {
	'Coinage' : 'cn',
	'Gast' : 'ga',
	'haven.resutil.Curiosity' : 'cu',
	'haven.ItemInfo$Contents' : 'co',
	'Ingredient' : 'in',
	'haven.resutil.FoodInfo' : 'fo',
	'Slotted' : 'sl',
	'haven.res.ui.tt.slots.ISlots' : 'is',
	'haven.res.ui.tt.attrmod.AttrMod' : 'at',
	'Grievous' : 'gr',
	'Armpen' : 'ap',
	'Weight' : 'we',
	'Damage' : 'dm',
	'haven.res.ui.tt.Wear' : 'wr',
	'haven.res.ui.tt.Armor' : 'ar',
	'haven.ItemInfo$AdHoc' : 'ah',
	'Uses' : 'us',
	'Expiring' : 'ex',
	'Deglut' : 'dg',
	'Levelup' : 'lv',
	'haven.GItem$Amount' : 'am'
};

const round = (f) => Math.round(f * 100) / 100;
const lround = (f) => Math.round(f * 10000) / 10000;
const attrS = (s) => s.toLowerCase().slice(0, 3).replace('per', 'prc').replace('cha', 'csm') + s.slice(-1);

const writeUnicodeJSON = (jsobject, filename, isAppend) => {
	const jOutputStreamWriter = Java.type('java.io.OutputStreamWriter');
	const jFileOutputStream = Java.type('java.io.FileOutputStream');
	const jCharset = Java.type('java.nio.charset.Charset');
	const jPrintWriter = Java.type('java.io.PrintWriter');
	try { 
		let osw = new jOutputStreamWriter(new jFileOutputStream(filename, isAppend),
			jCharset.forName("UTF-8").newEncoder());
		let pw = new jPrintWriter(osw, true);
		pw.println( JSON.stringify(jsobject) );
		pw.close();
	} catch (e) {
	    err(`writeFile: ${e}`);
	}
}
const javaListToArray = (javalist) => {
	const ret = [];
	for(let i=0; i<javalist.size(); i++)
		ret.push(javalist.get(i));
	return ret;
};
const cancelAction = () => PBotUtils.mapClick(PBotGobAPI.player().gob.rc.floor().x, PBotGobAPI.player().gob.rc.floor().y, 1, 0);

function processBS(PBGob, beaconRC) {
	PBGob.doClick(3, 0);
	let successRead = null;
	let retriesNum = 10;
	do {
		try {
			successRead = checkBStand(PBGob.gob.rc.sub(beaconRC));
		} catch (e) {
			if ( e.toString().indexOf('haven.Loading') != -1 )
				err(`LOADING... ${retriesNum}`);
			err(e);
			successRead = null;
			retriesNum--;
			PBotUtils.sleep(1000);
		}
	} while ((successRead == null) && (retriesNum >= 0));
	if (successRead.length > 0) 
		return successRead;
	else
		return null;
}

function checkBStand(rc) {
	let ret = null;
	let wnd = waitWindow('Barter Stand', 60 * 1000);
	if (!wnd) {
		err(`NO WINDOW FOUND`);
		return ret;
	}
	let arraySB = getShopboxArray(wnd);
	ret = [];
	for (let s of arraySB)
		ret.push(processShopbox(s, rc));
	return ret;
}

function getShopboxArray(w) {
	const hcShopbox = Java.type('haven.res.ui.barterbox.Shopbox');
	let shopboxes = [];
	for (let wdg = w.lchild; wdg; wdg = wdg.prev)
		if (wdg instanceof hcShopbox)
			shopboxes.push(wdg);
	
	return shopboxes.filter( (a) => a.price && a.res && a.pnum > 0);
}

function processShopbox(s, rc) {
	let ret = {
		c : {},
		p : {},
		b : {
			x : round(rc.x),
			y : round(rc.y)
		}
	};

	let bi = processInfo(s.info().toArray());

	ret.c.r = s.res.res.get().name;
	ret.c.l = s.num.text.split(" ")[0];
	ret.c.n = bi.n;
	ret.c.q = bi.q;

	ret.p.n = s.price.name();
	ret.p.q = s.pq;
	ret.p.a = s.pnum;
	ret.p.r = s.price.res.res.get().name;

	let cExtra = processExtra(s.info().toArray());
	let pExtra = processExtra(s.price.info().toArray());
	if (cExtra)
		ret.c.e = cExtra;
	if (pExtra)
		ret.p.e = pExtra;

	return ret;
}

function processInfo(a) {
	let ret = {};

	for (let i of a) {
		let className = i.getClass().getName();
		let alias = aliases[className];
		let value = null;

		switch (className) {
			case 'haven.ItemInfo$Name' : 
				value = i.str.text;				
				break;
			case 'Quality' : 
				value = round(i.q);				
				break;
			
			default: 
				if (!aliasesExtra[className])
					d(`Unknown class ${className}`);
		}
		if (value && alias) ret[alias] = value;
	}
	return ret;
}

function processExtra(a) {
	let ret = [];

	for (let i of a) {
		let className = i.getClass().getName();
		let alias = aliasesExtra[className];
		let value = null;

		switch (className) {
			case 'haven.GItem$Amount' :
				value = i.itemnum();
				break;
			case 'haven.res.ui.tt.Wear' : 
				value = {
					dw : i.d,
					mw : i.m,
				}
				break;
			case 'haven.res.ui.tt.Armor' : 
				value = {
					ha : i.hard,
					sa : i.soft,
				}
				break;
			case 'Grievous' : 
				value = i.deg;
				break;
			case 'Armpen' : 
				value = i.deg;
				break;
			case 'Weight' : 
				value = i.attr.basename();
				break;
			case 'Damage' : 
				value = i.dmg;
				break;
			case 'Coinage' : 
				value = i.nm;
				break;
			case 'haven.ItemInfo$AdHoc' : 
				value = i.str.text;
				break;
			case 'Levelup' : 
				value = i.res.basename();
				break;
			case 'Uses' : 
				value = i.num;
				break;
			case 'haven.ItemInfo$Contents' : 
				value = processInfo( i.sub.toArray() );
				break;
			case 'Deglut' : 
			value = {
				inc : lround(i.a),
				min : i.min
			}
			break;			
			case 'Expiring' : 
				value = {
					st : i.stime,
					et : i.etime,
					me : lround(i.meter())
				}
				break;
			case 'Ingredient' : 
				value = {
					n : i.name,
					v : round(i.val)
				}
				break;
			case 'haven.resutil.FoodInfo' : 
				value = {
					en : lround(i.end),
					hu : lround(i.glut),
					ev : processSubarray(i.evs)
				}
				break;
			case 'Gast' : 
				value = {
					hm : lround(i.glut),
					fm : lround(i.fev)
				}
				break;
			case 'haven.resutil.Curiosity' : 
				value = {
					lp : i.exp,
					xp : i.enc,
					mw : i.mw,
					tm : round(i.time)
				}
				break;
			case 'Slotted' : 
				value = processSubarray( i.sub.toArray() )[0].v;
				break;
			case 'haven.res.ui.tt.slots.ISlots' : 
				value = {
					le : i.left,
					it : processSubarray( i.s.toArray() )
				}
				break;
			case 'haven.res.ui.tt.attrmod.AttrMod' : 
				value = processSubarray( i.mods.toArray() );
				break;
			default: 
				if (!aliases[className])
					d(`Unknown extra class ${className}`);
		}
		if (value && alias) ret.push({n : alias, v : value});
	}	
	return ret;
}

function processSubarray(a) {
		let ret = [];
		for (let i of a) {
			let className = i.getClass().getName();
			let alias = null;
			let value = null;

			switch (className) {
				case 'haven.res.ui.tt.slots.ISlots$SItem' : 
					alias = i.name;
					value = processSubarray(i.info.toArray())[0].v;
					break;
				case 'haven.res.ui.tt.attrmod.AttrMod$Mod' : 
					alias = i.attr.basename();
					value = i.mod;
					break;
				case 'haven.res.ui.tt.attrmod.AttrMod' : 
					alias = 'mods';
					value = processSubarray(i.mods);
					break;
				case 'haven.resutil.FoodInfo$Event' : 
					alias = attrS(i.ev.nm);
					value = round(i.a);
					break;
				default: 
					d(`Unknown subarray class ${className}`);
			}
			if (value && alias) ret.push({n : alias, v : value});
		}	
		return ret;
	}

function waitWindow(s, t) {
	const TICK = 100;
	let r = null;
	let c = 0;
	while ((r == null) && (c < t)) {
		r = PBotWindowAPI.getWindow(s);
		PBotUtils.sleep(TICK);
		c += TICK;
	}
	return r;
}