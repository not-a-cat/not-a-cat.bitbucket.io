const PBotAPI = Java.type('haven.purus.pbot.PBotAPI');
const PBotGobAPI = Java.type('haven.purus.pbot.PBotGobAPI');
const PBotWindowAPI = Java.type('haven.purus.pbot.PBotWindowAPI');
const PBotUtils = Java.type('haven.purus.pbot.PBotUtils');
const hcCoord2d = Java.type('haven.Coord2d');
const hcCoord = Java.type('haven.Coord');
const hcResource = Java.type('haven.Resource');
const hcMessage = Java.type('haven.Message');
const hcResDrawable = Java.type('haven.ResDrawable');

load("Barter.js");

const WINDOWNAME = "WP Runner";
const ACTION1 = "Select beacon";
const ACTION2 = "Load waypoints";
const ACTION3 = "> >  RUN  > >";
const GOBIDSTART = -900000;
const TILESZ = 11.0;
const RESNAME = "gfx/terobjs/arch/visflag";
const FILEPATH = "dump/";

var MARKETNAME = 'Lucky Market';
var BEACON = null;
var OFFSETLIST = [];
var BSIGNOREID = [];
var LOG = {
	marketInfo : {},
	data : []
};

const d = (i) => PBotUtils.sysMsg(`${i}`);
const err = (i) => PBotUtils.sysMsg(`${i}`, 255, 128, 0);

const stop = () => mWindow.closed();

const readFile = () => { //'x, y' lines from file to [Coord2d] array
	let ret = [];
	let FILENAME = `${FILEPATH}test.wpl`;
	const jFileReader = Java.type('java.io.FileReader');
	const jBufferedReader = Java.type('java.io.BufferedReader');
	
	let br = new jBufferedReader(new jFileReader(FILENAME));
	try {
	    let line = br.readLine();
	    while (line != null) {
	        let coords = line.split(", ");
	        let x = parseFloat(coords[0]);
	        let y = parseFloat(coords[1]);
	        if (!isNaN(x) && !isNaN(y))
		        ret.push(new hcCoord2d(x, y));
	        line = br.readLine();
	    }
	} finally {
	    br.close();
	}
	return ret;
}

//UI
const mWindow = PBotUtils.PBotWindow(WINDOWNAME, 120, 110, ScriptID);

const f1b = mWindow.addButton("f1", ACTION1, 64, 5, 5);
const f2b = mWindow.addButton("f2", ACTION2, 64, 5, 50);
const f3b = mWindow.addButton("f3", ACTION3, 64, 5, 95);

const infoLabel1 = mWindow.addLabel("", 5, 30);
const infoLabel2 = mWindow.addLabel("", 5, 75);

const f1 = () => {
	d(setBeacon());
	infoLabel1.setText(`Beacon: ${BEACON.getGobId()}`);
};

const f2 = () => {
	OFFSETLIST = readFile();
	infoLabel2.setText(`Total WPs: ${OFFSETLIST.length} loaded`);
};

const f3 = () => d( run() );

const run = () => {
	if (!BEACON)
		return `No beacon set`;
	if (!OFFSETLIST[0])
		return `Empty WP list`;
	let now = new Date();
	LOG.marketInfo.timestamp = now;
	LOG.marketInfo.name = MARKETNAME;
	LOG.data = [];
	BSIGNOREID = [];

	let wpArray = [];
	for (let c of OFFSETLIST)
		wpArray.push(c.add(BEACON.gob.rc));

	for (let c of wpArray) {
		if (stop()) return `Interrupted`;
		assistedMovement(c, true);
		processArea(false);
	}

	let timemark = now.toJSON().replace(/[:,T,-]/g, '').slice(0, 14);
	let jsonOutFilename = `${FILEPATH}${timemark}.json`;
	writeUnicodeJSON(LOG, jsonOutFilename, false);
	return `${ScriptID} run() finished`;
}

const assistedMovement = (c, pf) => {
	let TO = 60 * 1000;
	let tick = 250;
	let waymark = placeMark(c);
	if (pf) PBotUtils.pfLeftClick(c.x, c.y);
	PBotUtils.mapClick(c.floor().x, c.floor().y, 1, 0);
	while (PBotGobAPI.player().gob.rc.dist(c) > 6) {
		TO -= tick;
		PBotUtils.sleep(tick);
	}
	if ( PBotGobAPI.player().gob.rc.dist(c) > 6 )
		manualMovementRequest();
	removeMark(waymark);
}

const manualMovementRequest = () => {
	d("Please move to the red flag and close pause window");
	let pauseWindow = PBotUtils.PBotWindow(`-- ${WINDOWNAME} PAUSED --`, 15, 300, ScriptID);
	while ( !pauseWindow.closed() )
		PBotUtils.sleep(1000);
}

const processArea = (skip) => {
	let nextbs = getGobsByResname('gfx/terobjs/barterstand', BSIGNOREID, TILESZ * 7.5)[0];
	while (nextbs) {
		if (stop()) return true;
		let c = new hcCoord2d(25.0, 0.0);
		c = c.rotate(nextbs.gob.a).add(nextbs.gob.rc);
		assistedMovement(c, false);
		if (!skip) {
			let dataChunk = processBS(nextbs, BEACON.gob.rc);
			if (dataChunk)
				LOG.data = LOG.data.concat(dataChunk);
		}
		BSIGNOREID.push( nextbs.getGobId() );
		assistedMovement(c, false);
		nextbs = getGobsByResname('gfx/terobjs/barterstand', BSIGNOREID, TILESZ * 2.5, nextbs.gob.rc)[0]; //search for unignore BS in radius around checked BS
	}
}

const setBeacon = () => {
	d(`Alt+LMB beacon`);
	BEACON = PBotGobAPI.selectGob();
	if (!BEACON)
		return `Nothing selected`;
	return `${BEACON.getResname()}, rc: ${BEACON.gob.rc.floor()}`;
};

const removeMark = (g) => PBotAPI.gui.ui.sess.glob.oc.remove(g.getGobId());

function placeMark(c) {
	const hcGob = Java.type('haven.Gob');
	const hcOverlay = hcGob.Overlay;
	const hcGobHitbox = Java.type('haven.GobHitbox');
	let gobId = GOBIDSTART;
	while (PBotGobAPI.findGobById(gobId))
		gobId--;

	const loadedRes = hcResource.remote().load(RESNAME);
	let ui = PBotAPI.gui.ui;
	let g = ui.sess.glob.oc.getgob(gobId, 0);
	ui.sess.glob.oc.move(g, c, Math.PI/2);
	let newattr = new hcResDrawable(g, loadedRes, hcMessage.nil);
	g.setattr(newattr);
	let aOL = [
		new hcOverlay(new hcGobHitbox(g, new hcCoord(-3, -3), new hcCoord(3, 3), true))
	];
	for (let ol of aOL)
		g.ols.add(ol);
	return PBotGobAPI.findGobById(gobId);
}

function getGobsByResname(s, u, r, t) {
	let res = [];
	let done = false;
	let TO = 10;
	if (!r) r = TILESZ * 80;
	if (!t) t = PBotGobAPI.player().gob.rc;
	do {
		try {
				let gobs = PBotGobAPI.getAllGobs();
				for (let g of gobs) {
					if ( !g.getResname() )
						continue;
					if (g.getResname() == s)
						res.push(g);
				}
				done = true;
		} catch (e) {
			res = [];
			done = false;
			dbg(`getGobsByResname: ${e}`);
			TO--;
			PBotUtils.sleep(TICK);
		}
	} while ((!done) & (TO >= 0));
	if (u)
		res = res.filter( a => u.indexOf( a.getGobId() ) == -1);
	res = res.filter( a => a.gob.rc.dist(t) <= r );
	res.sort( (a, b) => a.gob.rc.dist(t) - b.gob.rc.dist(t) );
	return res;
}