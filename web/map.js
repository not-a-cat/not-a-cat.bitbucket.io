function mapMarksRegen(array, classname) {
	var existing = document.getElementsByClassName(classname);
	for (var i = 0; i < existing.length; i++)
		map.removeChild(existing[i]);
	var standIcons = [];
	for (var i = 0; i < array.length; i++) {
		var standIcon = {
			x : array[i].b.x,
			y : array[i].b.y
		}
		var isNew = true;
		for (var j = 0; j < standIcons.length; j++) {
			if (
				(standIcons[j].x == standIcon.x)
				 &&
				(standIcons[j].y == standIcon.y)
			)
				isNew = false;
		}
		if (isNew) 
			standIcons.push(standIcon);
	}

	for (var i = 0; i < standIcons.length; i++)
		mapAddMark(classname, rc2s(standIcons[i]));
}

function mapAddMark(type, c) {
var ns = "http://www.w3.org/2000/svg";
	switch (type) {
		case "SVGmarker" : 
			var mark = document.createElementNS(ns, 'circle');
			mark.setAttribute('cx', c.x);
			mark.setAttribute('cy', c.y);
			mark.setAttribute('r', 5);
			mark.setAttribute('id', 'SVGmarker');
			break;
		case "SVGstand" : 
			var r = 2.25;
			var mark = document.createElementNS(ns, 'polygon');
			mark.setAttribute('points', octagon(c.x, c.y, r));
			break;
		case "SVGstandInactive" : 
			var r = 2.5;
			var mark = document.createElementNS(ns, 'polygon');
			mark.setAttribute('points', octagon(c.x, c.y, r));
			break;
		default:  console.log(type);
			return;
	}
	mark.setAttribute('class', type);
	map.appendChild(mark);
	return mark;
}

function octagon(cx, cy, r) {
	var a = r/1.8;
	var q = Math.sqrt(r*r/2)+a;
	var pointsArray = [
		[a, -q],
		[q, -a],
		[q, a],
		[a, q],
		[-a, q],
		[-q, a],
		[-q, -a],
		[-a, -q]
	];
	var pointsString = "";
	for (var i = 0; i < pointsArray.length; i++)
		pointsString += (pointsArray[i][0] + cx) + "," + (pointsArray[i][1] + cy) + " ";
	return pointsString;
}