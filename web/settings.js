var resURL = "http://www.havenandhearth.com/mt/r/";
var jsonURL = "https://api.npoint.io/35e655187899ab0eb851";

var modx = 3;
var mody = 3;
var offx = 149.5;
var offy = 149.5;

var aBonuses = {
	"agi" : "Agility",
	"csm" : "Charisma",
	"con" : "Constitution",
	"dex" : "Dexterity",
	"int" : "Intelligence",
	"prc" : "Perception",
	"psy" : "Psyche",
	"str" : "Strength",
	"wil" : "Will",
	"carpentry" : "Carpentry",
	"cooking" : "Cooking",
	"explore" : "Exploration",
	"farming" : "Farming",
	"lore" : "Lore",
	"masonry" : "Masonry",
	"melee" : "Melee Combat",
	"ranged" : "Marksmanship",
	"sewing" : "Sewing",
	"smithing" : "Smithing",
	"stealth" : "Stealth",
	"survive" : "Survival",
	"unarmed" : "Unarmed Combat",
	"expmod" : "Learning Ability",
	"swim" : "Swimming"
}

var aFEP = {
	"agi1" : "Agility",
	"csm1" : "Charisma",
	"con1" : "Constitution",
	"dex1" : "Dexterity",
	"int1" : "Intelligence",
	"prc1" : "Perception",
	"psy1" : "Psyche",
	"str1" : "Strength",
	"wil1" : "Will",
	"agi2" : "Agility +2",
	"csm2" : "Charisma +2",
	"con2" : "Constitution +2",
	"dex2" : "Dexterity +2",
	"int2" : "Intelligence +2",
	"prc2" : "Perception +2",
	"psy2" : "Psyche +2",
	"str2" : "Strength +2",
	"wil2" : "Will +2"
}